import fetch from 'isomorphic-fetch'

export class DataObject {
	static get apiUrl() {
		return 'https://soccerbackend.sergey-panov.ru/';
	}
	static get url() {
		return "test";
	}
	static async getData(slugs = []) {
		if (slugs.length) 
			slugs.unshift("");
		const res = await fetch(this.apiUrl+this.url+slugs.join("/"))
		console.log(res.url);
	    const data = await res.json()
	    return data
	}
}

export class Teams extends DataObject {
	static get url() {
		return "teams";
	}
}

export class Players extends DataObject {
	static get url() {
		return "players";
	}
}

export class Games extends DataObject {
	static get url() {
		return "games";
	}
}

export class Stadiums extends DataObject {
	static get url() {
		return "stadiums";
	}
}