/* global window */
import Link from 'next/link'
import React from 'react'
import { Row, Col, Nav, NavItem, NavLink, Card, CardHeader, CardBody, CardTitle, CardSubtitle, CardText } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'
import { Teams, Games } from '../components/async-data'
import date from 'date-and-time';

export default class extends Page {

  /* eslint no-undefined: "error" */
  static async getInitialProps({req, query}) {
    // Inherit standard props from the Page (i.e. with session data)
    let props = await super.getInitialProps({req})
    props.id = query.id

    // If running on server, perform Async call
    if (typeof window === 'undefined') {
      try {
        props.data = await Games.getData([props.id])
      } catch (e) {
        props.error = "Unable to fetch Game on server"
      }
    }

    return props
  }

  // Set posts on page load (only if prop is populated, i.e. running on server)
  constructor(props) {
    super(props)
    this.state = {
      data: props.data || null,
      error: props.error || null
    }
  }

  // This is called after rendering, only on the client (not the server)
  // This allows us to render the page on the client without delaying rendering,
  // then load the data fetched via an async call in when we have it.
  async componentDidMount() {
    // Only render posts client side if they are not populate (if the page was 
    // rendered on the server, the state will be inherited from the server 
    // render by the client)
    if (this.state.data === null) {
      try {
        this.setState({
          data: await Games.getData([props.id]),
          error: null
        })
      } catch (e) {
        this.setState({
          error: "Unable to fetch Game on client"
        })
      }
    }
  }

  render() {
    return (
      <Layout {...this.props} navmenu={false}>
        <RenderGame data={this.state.data} error={this.state.error}/>
      </Layout>
    )
  }

}

export class RenderGame extends React.Component {
  render() {
    if (this.props.error) {
      // Display error if posts have fialed to load
      return <p><span className="font-weight-bold">Error loading posts:</span> {this.props.error}</p>
    } else if (!this.props.data) {
      // Display place holder if posts are still loading (and no error)
      return <p><i>Loading content…</i></p>
    } else {
      // Display posts
      var game = this.props.data.response;
      return <React.Fragment>
        {
          <div>
            <Row>
              <Col sm="6"><Link href={"/teams/"+game.team1}><a className="h1">{game.team1_name}</a></Link></Col>
              <Col sm="6" className="text-right"><Link href={"/teams/"+game.team2}><a className="h1">{game.team2_name}</a></Link></Col>
            </Row>
            <h1 className="display-1 text-center font-weight-bold">
              {game.team1_goals}:{game.team2_goals} <span className="font-weight-bold">{game.last_name}</span> {game.tshirt == null ? '' : <span className="font-weight-bold">({game.tshirt})</span>} <span className="badge badge-dark">{game.numberInTeam}</span>
            </h1>
            <div className="text-center mb-5">
              <h4>Тур {game.tour}</h4>
              <h4>{date.format(date.parse(game.game_date.slice(0, -6), "YYYY-MM-DDThh:mm:ss"), "DD.MM.YYYY, HH:mm")}</h4>
              <Link href={"/stadiums/"+game.stadium}><a className="h4">{game.stadium_name}</a></Link>
            </div>
            <GameEvents gameId={game.id}/>
          </div>
        }
      </React.Fragment>
    }
  }
}

export class TeamName extends React.Component {
  // Set posts on page load (only if prop is populated, i.e. running on server)
  constructor(props) {
    super(props)
    this.state = {
      teamId: props.teamId || null,
      data: props.data || null,
      error: props.error || null
    }
  }
  async componentDidMount() {
    // Only render posts client side if they are not populate (if the page was 
    // rendered on the server, the state will be inherited from the server 
    // render by the client)
    if (this.state.data === null) {
      try {
        this.setState({
          data: await Teams.getData([this.state.teamId]),
          error: null
        })
      } catch (e) {
        this.setState({
          error: "Unable to fetch Team on client"
        })
      }
    }
  }
  render() {
    return (
      <React.Fragment>
        {this.state.data == null ? <h3>...</h3> : <Link href={"/teams/"+this.state.data.response.id}><a className="h3 ">{this.state.data.response.name}</a></Link>}
      </React.Fragment>
    )
  }
}

export class GameEvents extends React.Component {
  // Set posts on page load (only if prop is populated, i.e. running on server)
  constructor(props) {
    super(props)
    this.state = {
      gameId: props.gameId || null,
      data: props.data || null,
      error: props.error || null
    }
  }
  async componentDidMount() {
    // Only render posts client side if they are not populate (if the page was 
    // rendered on the server, the state will be inherited from the server 
    // render by the client)
    if (this.state.data === null) {
      try {
        this.setState({
          data: await Games.getData([this.state.gameId, "events"]),
          error: null
        })
      } catch (e) {
        this.setState({
          error: "Unable to fetch Team on client"
        })
      }
    }
  }
  render() {
    return (
      <React.Fragment>
        <RenderGameEvents data={this.state.data} error={this.state.error}/>
      </React.Fragment>
    )
  }
}

export class RenderGameEvents extends React.Component {
  render() {
    if (this.props.error) {
      // Display error if posts have fialed to load
      return <p><span className="font-weight-bold">Error loading posts:</span> {this.props.error}</p>
    } else if (!this.props.data) {
      // Display place holder if posts are still loading (and no error)
      return <p><i>Loading content…</i></p>
    } else {
      // var players = this.props.data.response;
      console.log(this.props.data.response)
      return (
        <Card>
          <CardHeader>Голы в игре</CardHeader>
          <CardBody>
            {
              this.props.data.response.items.map((e, i) => (
                <CardText key={i}>
                  {e.minute}' <Link href={"/players/"+e.by_player}><a className="text-dark"><span className="font-weight-bold">{e.by_player_last_name}</span> {e.by_player_first_name} {e.by_player_second_name}{e.by_player_tshirt == null ? '' : <span className="font-weight-bold"> ({e.by_player_tshirt})</span>}</a></Link> (<Link href={"/teams/"+e.by_team}><a className="text-dark">{e.by_team_name}</a></Link>)
                </CardText>
              ))
            }
          </CardBody>
        </Card>
      )
    }
  }
}