/* global window */
import Link from 'next/link'
import React from 'react'
import { Row, Col, Nav, NavItem, NavLink, Card, CardBody, CardTitle, CardSubtitle, CardText } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'
import { Teams } from '../components/async-data'

export default class extends Page {

  /* eslint no-undefined: "error" */
  static async getInitialProps({req}) {
    // Inherit standard props from the Page (i.e. with session data)
    let props = await super.getInitialProps({req})

    // If running on server, perform Async call
    if (typeof window === 'undefined') {
      try {
        props.data = await Teams.getData()
      } catch (e) {
        props.error = "Unable to fetch Teams on server"
      }
    }

    return props
  }

  // Set posts on page load (only if prop is populated, i.e. running on server)
  constructor(props) {
    super(props)
    this.state = {
      data: props.data || null,
      error: props.error || null
    }
  }

  // This is called after rendering, only on the client (not the server)
  // This allows us to render the page on the client without delaying rendering,
  // then load the data fetched via an async call in when we have it.
  async componentDidMount() {
    // Only render posts client side if they are not populate (if the page was 
    // rendered on the server, the state will be inherited from the server 
    // render by the client)
    if (this.state.data === null) {
      try {
        this.setState({
          data: await Teams.getData(),
          error: null
        })
      } catch (e) {
        this.setState({
          error: "Unable to fetch Teams on client"
        })
      }
    }
  }

  render() {
    return (
      <Layout {...this.props} title="Команды" navmenu={false}>
        <h1 className="display-2">Команды</h1>
        <Row>
          <RenderTeams data={this.state.data} error={this.state.error}/>
        </Row>
      </Layout>
    )
  }

}

export class RenderTeams extends React.Component {
  render() {
    if (this.props.error) {
      // Display error if posts have fialed to load
      return <Col xs="12" md="6"><span className="font-weight-bold">Ошибка:</span> {this.props.error}</Col>
    } else if (!this.props.data) {
      // Display place holder if posts are still loading (and no error)
      return <Col xs="12" md="6"><i>Загрузка…</i></Col>
    } else {
      // Display posts
      return <React.Fragment>
        {
          this.props.data.response.items.map((team, i) => (
            <Col xs="12" md="6" key={i}>
              <Card className="mb-4">
                <CardBody>
                  <CardTitle><Link href={"/teams/"+team.id} prefetch><a className="text-dark font-weight-bold">{team.name}</a></Link></CardTitle>
                  <CardSubtitle>{team.city}</CardSubtitle>
                  <CardText>Тренер: <i>{team.coach_first_name} {team.coach_second_name} {team.coach_last_name}</i></CardText>
                </CardBody>
              </Card>
            </Col>
          ))
        }
      </React.Fragment>
    }
  }
}