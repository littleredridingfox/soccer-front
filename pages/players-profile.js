/* global window */
import Link from 'next/link'
import React from 'react'
import { Row, Col, Nav, NavItem, NavLink, Card, CardHeader, CardBody, CardTitle, CardSubtitle, CardText } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'
import { Teams, Players } from '../components/async-data'

export default class extends Page {

  /* eslint no-undefined: "error" */
  static async getInitialProps({req, query}) {
    // Inherit standard props from the Page (i.e. with session data)
    let props = await super.getInitialProps({req})
    props.id = query.id

    // If running on server, perform Async call
    if (typeof window === 'undefined') {
      try {
        props.data = await Players.getData([props.id])
      } catch (e) {
        props.error = "Unable to fetch Player on server"
      }
    }

    return props
  }

  // Set posts on page load (only if prop is populated, i.e. running on server)
  constructor(props) {
    super(props)
    this.state = {
      data: props.data || null,
      error: props.error || null
    }
  }

  // This is called after rendering, only on the client (not the server)
  // This allows us to render the page on the client without delaying rendering,
  // then load the data fetched via an async call in when we have it.
  async componentDidMount() {
    // Only render posts client side if they are not populate (if the page was 
    // rendered on the server, the state will be inherited from the server 
    // render by the client)
    if (this.state.data === null) {
      try {
        this.setState({
          data: await Players.getData([props.id]),
          error: null
        })
      } catch (e) {
        this.setState({
          error: "Unable to fetch Team on client"
        })
      }
    }
  }

  render() {
    return (
      <Layout {...this.props} navmenu={false}>
        <RenderPlayer data={this.state.data} error={this.state.error}/>
      </Layout>
    )
  }

}

export class RenderPlayer extends React.Component {
  render() {
    if (this.props.error) {
      // Display error if posts have fialed to load
      return <p><span className="font-weight-bold">Error loading posts:</span> {this.props.error}</p>
    } else if (!this.props.data) {
      // Display place holder if posts are still loading (and no error)
      return <p><i>Loading content…</i></p>
    } else {
      // Display posts
      var player = this.props.data.response;
      return <React.Fragment>
        {
          <div>
          <h1>
            {player.first_name} {player.second_name} <span className="font-weight-bold">{player.last_name}</span> {player.tshirt == null ? '' : <span className="font-weight-bold">({player.tshirt})</span>} <span className="badge badge-dark">{player.numberInTeam}</span>
          </h1>
          <span className="text-muted"><TeamName teamId={player.team} /></span>
            

          </div>
        }
      </React.Fragment>
    }
  }
}

export class TeamName extends React.Component {
  // Set posts on page load (only if prop is populated, i.e. running on server)
  constructor(props) {
    super(props)
    this.state = {
      teamId: props.teamId || null,
      data: props.data || null,
      error: props.error || null
    }
  }
  async componentDidMount() {
    // Only render posts client side if they are not populate (if the page was 
    // rendered on the server, the state will be inherited from the server 
    // render by the client)
    if (this.state.data === null) {
      try {
        this.setState({
          data: await Teams.getData([this.state.teamId]),
          error: null
        })
      } catch (e) {
        this.setState({
          error: "Unable to fetch Team on client"
        })
      }
    }
  }
  render() {
    return (
      <React.Fragment>
        {this.state.data == null ? <h3>...</h3> : <Link href={"/teams/"+this.state.data.response.id}><a className="h3 ">{this.state.data.response.name}</a></Link>}
      </React.Fragment>
    )
  }
}

export class TeamPlayers extends React.Component {
  // Set posts on page load (only if prop is populated, i.e. running on server)
  constructor(props) {
    super(props)
    this.state = {
      teamId: props.teamId || null,
      data: props.data || null,
      error: props.error || null
    }
  }
  async componentDidMount() {
    // Only render posts client side if they are not populate (if the page was 
    // rendered on the server, the state will be inherited from the server 
    // render by the client)
    if (this.state.data === null) {
      try {
        this.setState({
          data: await Teams.getData([this.state.teamId, "players"]),
          error: null
        })
      } catch (e) {
        this.setState({
          error: "Unable to fetch Team on client"
        })
      }
    }
  }
  render() {
    return (
      <React.Fragment>
        <RenderTeamPlayers data={this.state.data} error={this.state.error}/>
      </React.Fragment>
    )
  }
}

export class RenderTeamPlayers extends React.Component {
  render() {
    if (this.props.error) {
      // Display error if posts have fialed to load
      return <p><span className="font-weight-bold">Error loading posts:</span> {this.props.error}</p>
    } else if (!this.props.data) {
      // Display place holder if posts are still loading (and no error)
      return <p><i>Loading content…</i></p>
    } else {
      // var players = this.props.data.response;
      console.log(this.props.data.response)
      return (
        <Card>
          <CardHeader>Игроки</CardHeader>
          <CardBody>
            {
              this.props.data.response.items.map((player, i) => (
                <CardText key={i}>
                  {player.numberInTeam} <Link href={"/players/"+player.id}><a className="text-dark"><span className="font-weight-bold">{player.last_name}</span> {player.first_name} {player.second_name}</a></Link>
                </CardText>
              ))
            }
          </CardBody>
        </Card>
      )
    }
  }
}