/* global window */
import Link from 'next/link'
import React from 'react'
import { Row, Col, Nav, NavItem, NavLink, Card, CardHeader, CardBody, CardTitle, CardSubtitle, CardText } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'
import { Stadiums } from '../components/async-data'

export default class extends Page {

  /* eslint no-undefined: "error" */
  static async getInitialProps({req, query}) {
    // Inherit standard props from the Page (i.e. with session data)
    let props = await super.getInitialProps({req})
    props.id = query.id

    // If running on server, perform Async call
    if (typeof window === 'undefined') {
      try {
        props.data = await Stadiums.getData([props.id])
      } catch (e) {
        props.error = "Unable to fetch Team on server"
      }
    }

    return props
  }

  // Set posts on page load (only if prop is populated, i.e. running on server)
  constructor(props) {
    super(props)
    this.state = {
      data: props.data || null,
      error: props.error || null
    }
  }

  // This is called after rendering, only on the client (not the server)
  // This allows us to render the page on the client without delaying rendering,
  // then load the data fetched via an async call in when we have it.
  async componentDidMount() {
    // Only render posts client side if they are not populate (if the page was 
    // rendered on the server, the state will be inherited from the server 
    // render by the client)
    if (this.state.data === null) {
      try {
        this.setState({
          data: await Stadiums.getData([props.id]),
          error: null
        })
      } catch (e) {
        this.setState({
          error: "Unable to fetch Team on client"
        })
      }
    }
  }

  render() {
    return (
      <Layout {...this.props} navmenu={false}>
        <RenderStadium data={this.state.data} error={this.state.error}/>
      </Layout>
    )
  }

}

export class RenderStadium extends React.Component {
  render() {
    if (this.props.error) {
      // Display error if posts have fialed to load
      return <p><span className="font-weight-bold">Error loading posts:</span> {this.props.error}</p>
    } else if (!this.props.data) {
      // Display place holder if posts are still loading (and no error)
      return <p><i>Loading content…</i></p>
    } else {
      // Display posts
      var stadium = this.props.data.response;
      // console.log("stadium");
      // console.log(stadium);
      return <React.Fragment>
        {
          <div>
            <h1>
              {stadium.name}
            </h1>
            <h3 className="text-muted">{stadium.city}</h3>
            <p><strong>Вместимость:</strong> {stadium.capacity}</p>
          </div>
        }
      </React.Fragment>
    }
  }
}