/* global window */
import Link from 'next/link'
import React from 'react'
import { Row, Col, Nav, NavItem, NavLink, Card, CardBody, CardTitle, CardSubtitle, CardText } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'
import { Stadiums } from '../components/async-data'

export default class extends Page {

  /* eslint no-undefined: "error" */
  static async getInitialProps({req}) {
    // Inherit standard props from the Page (i.e. with session data)
    let props = await super.getInitialProps({req})

    // If running on server, perform Async call
    if (typeof window === 'undefined') {
      try {
        props.data = await Stadiums.getData()
      } catch (e) {
        props.error = "Unable to fetch Stadiums on server"
      }
    }

    return props
  }

  // Set posts on page load (only if prop is populated, i.e. running on server)
  constructor(props) {
    super(props)
    this.state = {
      data: props.data || null,
      error: props.error || null
    }
  }

  // This is called after rendering, only on the client (not the server)
  // This allows us to render the page on the client without delaying rendering,
  // then load the data fetched via an async call in when we have it.
  async componentDidMount() {
    // Only render posts client side if they are not populate (if the page was 
    // rendered on the server, the state will be inherited from the server 
    // render by the client)
    if (this.state.data === null) {
      try {
        this.setState({
          data: await Stadiums.getData(),
          error: null
        })
      } catch (e) {
        this.setState({
          error: "Unable to fetch Stadiums on client"
        })
      }
    }
  }

  render() {
    return (
      <Layout {...this.props} title="Стадионы" navmenu={false}>
        <h1 className="display-2">Стадионы</h1>
        <Row>
          <RenderStadiums data={this.state.data} error={this.state.error}/>
        </Row>
      </Layout>
    )
  }

}

export class RenderStadiums extends React.Component {
  render() {
    if (this.props.error) {
      // Display error if posts have fialed to load
      return <Col xs="12" md="6"><span className="font-weight-bold">Ошибка:</span> {this.props.error}</Col>
    } else if (!this.props.data) {
      // Display place holder if posts are still loading (and no error)
      return <Col xs="12" md="6"><i>Загрузка…</i></Col>
    } else {
      // Display posts
      
      return <React.Fragment>
        {
          this.props.data.response.items.map((stadium, i) => (
            <Col xs="12" md="6" key={i}>
              <Card>
                <CardBody>
                  <CardTitle><Link href={"/stadiums/"+stadium.id}><a className="text-dark font-weight-bold">{stadium.name}</a></Link></CardTitle>
                  <CardSubtitle>{stadium.city}</CardSubtitle>
                  <CardText>Вместимость: {stadium.capacity}</CardText>
                </CardBody>
              </Card>
            </Col>
          ))
        }
      </React.Fragment>
    }
  }
}