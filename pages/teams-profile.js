/* global window */
import Link from 'next/link'
import React from 'react'
import { Row, Col, Nav, NavItem, NavLink, Card, CardHeader, CardBody, CardTitle, CardSubtitle, CardText } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'
import { Teams, Players } from '../components/async-data'
import date from 'date-and-time';

export default class extends Page {

  /* eslint no-undefined: "error" */
  static async getInitialProps({req, query}) {
    // Inherit standard props from the Page (i.e. with session data)
    let props = await super.getInitialProps({req})
    props.id = query.id

    // If running on server, perform Async call
    if (typeof window === 'undefined') {
      try {
        props.data = await Teams.getData([props.id])
      } catch (e) {
        props.error = "Unable to fetch Team on server"
      }
    }

    return props
  }

  // Set posts on page load (only if prop is populated, i.e. running on server)
  constructor(props) {
    super(props)
    this.state = {
      data: props.data || null,
      error: props.error || null
    }
  }

  // This is called after rendering, only on the client (not the server)
  // This allows us to render the page on the client without delaying rendering,
  // then load the data fetched via an async call in when we have it.
  async componentDidMount() {
    // Only render posts client side if they are not populate (if the page was 
    // rendered on the server, the state will be inherited from the server 
    // render by the client)
    if (this.state.data === null) {
      try {
        this.setState({
          data: await Teams.getData([props.id]),
          error: null
        })
      } catch (e) {
        this.setState({
          error: "Unable to fetch Team on client"
        })
      }
    }
  }

  render() {
    return (
      <Layout {...this.props} navmenu={false}>
        <RenderTeam data={this.state.data} error={this.state.error}/>
      </Layout>
    )
  }

}

export class RenderTeam extends React.Component {
  render() {
    if (this.props.error) {
      // Display error if posts have fialed to load
      return <p><span className="font-weight-bold">Error loading posts:</span> {this.props.error}</p>
    } else if (!this.props.data) {
      // Display place holder if posts are still loading (and no error)
      return <p><i>Loading content…</i></p>
    } else {
      // Display posts
      var team = this.props.data.response;
      // console.log("team");
      // console.log(team);
      return <React.Fragment>
        {
          <div>
          <h1>
            {team.name}
          </h1>
          <h3 className="text-muted">{team.city}</h3>
          <div className="mb-3">
          <p className="mb-0"><strong>Главный тренер:</strong> {team.coach_first_name} {team.coach_second_name} {team.coach_last_name}</p>
          <p className="mb-0"><strong>Место в таблице прошлого сезона:</strong> {team.prevseason}</p>
          </div>
          <TeamGames teamId={team.id} />
          <TeamPlayers teamId={team.id} />
          <Card className="mb-3">
            <CardHeader>Домашний стадион</CardHeader>
            <CardBody>
              <CardTitle>{team.stadium_name}</CardTitle>
              <CardSubtitle>{team.stadium_city}</CardSubtitle>
              <CardText><strong>Вместимость:</strong> {team.stadium_capacity}</CardText>
            </CardBody>
          </Card>
          </div>
        }
      </React.Fragment>
    }
  }
}

export class TeamPlayers extends React.Component {
  // Set posts on page load (only if prop is populated, i.e. running on server)
  constructor(props) {
    super(props)
    this.state = {
      teamId: props.teamId || null,
      data: props.data || null,
      error: props.error || null
    }
  }
  async componentDidMount() {
    // Only render posts client side if they are not populate (if the page was 
    // rendered on the server, the state will be inherited from the server 
    // render by the client)
    if ((this.state.data === null) && (this.state.teamId != null)) {
      // console.log(this.state)
      try {
        this.setState({
          data: await Teams.getData([this.state.teamId, "players"]),
          error: null
        })
      } catch (e) {
        this.setState({
          error: "Unable to fetch Team on client"
        })
      }
    }
  }
  render() {
    return (
      <React.Fragment>
        <RenderTeamPlayers data={this.state.data} error={this.state.error}/>
      </React.Fragment>
    )
  }
}

export class TeamGames extends React.Component {
  // Set posts on page load (only if prop is populated, i.e. running on server)
  constructor(props) {
    super(props)
    this.state = {
      teamId: props.teamId || null,
      data: props.data || null,
      error: props.error || null
    }
  }
  async componentDidMount() {
    // Only render posts client side if they are not populate (if the page was 
    // rendered on the server, the state will be inherited from the server 
    // render by the client)
    if ((this.state.data === null) && (this.state.teamId != null)) {
      // console.log(this.state)
      try {
        this.setState({
          data: await Teams.getData([this.state.teamId, "games"]),
          error: null
        })
      } catch (e) {
        this.setState({
          error: "Unable to fetch Team on client"
        })
      }
    }
  }
  render() {
    return (
      <Row className="mb-3">
        <RenderTeamGames data={this.state.data} error={this.state.error}/>
      </Row>
    )
  }
}

export class RenderTeamPlayers extends React.Component {
  render() {
    if (this.props.error) {
      // Display error if posts have fialed to load
      return <p><span className="font-weight-bold">Error loading posts:</span> {this.props.error}</p>
    } else if (!this.props.data) {
      // Display place holder if posts are still loading (and no error)
      return <p><i>Загрузка списка игроков…</i></p>
    } else {
      // var players = this.props.data.response;
      console.log(this.props.data.response)
      return (
        <Card className="mb-3">
          <CardHeader>Игроки</CardHeader>
          <CardBody>
            {
              this.props.data.response.items.map((player, i) => (
                <CardText key={i}>
                  {player.numberInTeam} <Link href={"/players/"+player.id}><a className="text-dark"><span className="font-weight-bold">{player.last_name}</span> {player.first_name} {player.second_name} {player.tshirt == null ? '' : <span className="font-weight-bold">({player.tshirt})</span>}</a></Link>
                </CardText>
              ))
            }
          </CardBody>
        </Card>
      )
    }
  }
}


export class RenderTeamGames extends React.Component {
  render() {
    if (this.props.error) {
      // Display error if posts have fialed to load
      return <Col xs="12" md="6"><span className="font-weight-bold">Ошибка:</span> {this.props.error}</Col>
    } else if (!this.props.data) {
      // Display place holder if posts are still loading (and no error)
      return <Col xs="12" md="6"><i>Загрузка игр команды…</i></Col>
    } else {
      // Display posts
      console.log(this.props.data.response)
      return <React.Fragment>
        {
          this.props.data.response.items.map((game, i) => (
            <Col xs="12" md="6" key={i}>
              <Card>
                <CardBody>
                  <CardTitle><Link href={"/games/"+game.id}><a className="text-dark font-weight-bold">{game.team1_name} — {game.team2_name}</a></Link></CardTitle>
                  <CardSubtitle>Тур {game.tour}. {date.format(date.parse(game.game_date.slice(0, -6), "YYYY-MM-DDThh:mm:ss"), "DD.MM.YYYY, HH:mm")}. {game.stadium_name}</CardSubtitle>
                  <h1 className="mb-0">{game.team1_goals}:{game.team2_goals}</h1>
                  <CardText></CardText>
                </CardBody>
              </Card>
            </Col>
          ))
        }
      </React.Fragment>
    }
  }
}